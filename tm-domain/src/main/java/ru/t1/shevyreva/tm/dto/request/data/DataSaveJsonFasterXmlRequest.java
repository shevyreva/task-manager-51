package ru.t1.shevyreva.tm.dto.request.data;

import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

public class DataSaveJsonFasterXmlRequest extends AbstractUserRequest {

    public DataSaveJsonFasterXmlRequest(@Nullable String token) {
        super(token);
    }

}
