package ru.t1.shevyreva.tm.dto.response.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.shevyreva.tm.dto.response.AbstractResultResponse;

@Getter
@Setter
@NoArgsConstructor
public class UserLoginResponse extends AbstractResultResponse {

    @NotNull
    private String token;

    public UserLoginResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

    public UserLoginResponse(@NotNull final String token) {
        this.token = token;
    }

}
