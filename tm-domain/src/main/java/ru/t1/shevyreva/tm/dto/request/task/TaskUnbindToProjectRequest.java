package ru.t1.shevyreva.tm.dto.request.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.shevyreva.tm.dto.request.AbstractUserRequest;

@Getter
@Setter
@NoArgsConstructor
public class TaskUnbindToProjectRequest extends AbstractUserRequest {

    @Nullable
    private String taskId;

    @Nullable
    private String projectId;

    public TaskUnbindToProjectRequest(@Nullable String token, @Nullable String taskId, @Nullable String projectId) {
        super(token);
        this.taskId = taskId;
        this.projectId = projectId;
    }

}
